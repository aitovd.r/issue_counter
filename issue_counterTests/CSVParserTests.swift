//
//  CSVParserTests.swift
//  issue_counterTests
//
//  Created by aitovd.r@gmail.com on 20/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import XCTest
@testable import issue_counter

class CSVParserTests: XCTestCase {
    
    var sut: CSVParser!
    var listener: TastableCSVListener!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        if let issuesFileUrl = Bundle.main.url(forResource: "issues-test", withExtension: "csv") {
            listener = TastableCSVListener()
            sut = CSVParser(file: issuesFileUrl, listener: listener)
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCSVParserCallListenerMethods() {
        do {
            try sut.parse()
        } catch {
            
        }
        
        sleep(1)
        
        XCTAssertTrue(listener.didStartParsingFileCalled)
        XCTAssertTrue(listener.didParseHeadersCalled)
        XCTAssertTrue(listener.didParseRowsCalled)
        XCTAssertTrue(listener.didFinishParsingFileCalled)
        
    }
    
    func testCSVParserReturnCorrectHeader() {
        do {
            try sut.parse()
        } catch {
            
        }
        
        sleep(1)
        
        XCTAssertTrue(listener.didParseHeadersCalled)
        XCTAssertEqual(listener.headers.count, 4)
        XCTAssertEqual(listener.headers[0], "First name")
        XCTAssertEqual(listener.headers[1], "Sur name")
        XCTAssertEqual(listener.headers[2], "Issue count")
        XCTAssertEqual(listener.headers[3], "Date of birth")
    }
    
    func testCSVParserReturnCorrectRows() {
        do {
            try sut.parse()
        } catch {
            
        }
        
        sleep(1)
        
        XCTAssertTrue(listener.didParseHeadersCalled)
        XCTAssertEqual(listener.rows.count, 2)
        
        XCTAssertEqual(listener.rows[0]["First name"], "Theo")
        XCTAssertEqual(listener.rows[0]["Sur name"], "Jansen")
        XCTAssertEqual(listener.rows[0]["Issue count"], "5")
        XCTAssertEqual(listener.rows[0]["Date of birth"], "1978-01-02T00:00:00")
        
        XCTAssertEqual(listener.rows[1]["First name"], "Fiona")
        XCTAssertEqual(listener.rows[1]["Sur name"], "de Vries")
        XCTAssertEqual(listener.rows[1]["Issue count"], "7")
        XCTAssertEqual(listener.rows[1]["Date of birth"], "1950-11-12T00:00:00")
    }
    
    func testCSVParserThrowExeptionOnNonFileURL() {
        sut = CSVParser(file: URL(string: "https://google.com")!, listener: listener)
        XCTAssertThrowsError(try sut.parse()) { error in
            XCTAssertEqual(error as? CSVParserError, .notFileURL)
        }
    }
    
    class TastableCSVListener : CSVParserListener {
        var didParseHeadersCalled = false
        var headers = [String]()
        
        var didParseRowsCalled = false
        var rows = [[String : String]]()
        
        var didFinishParsingFileCalled = false
        
        var didStartParsingFileCalled = false
        
        func parser(_ parser: CSVParser, didParseHeaders headers: [String]) {
            didParseHeadersCalled = true
            self.headers = headers
        }
        
        func parser(_ parser: CSVParser, didParseRows rows: [[String : String]]) {
            didParseRowsCalled = true
            self.rows = rows
        }
        
        func parser(_ parser: CSVParser, didFinishParsingFile file: URL) {
            didFinishParsingFileCalled = true
        }
        
        func parser(_ parser: CSVParser, didStartParsingFile file: URL) {
            didStartParsingFileCalled = true
        }
    }
    
}
