//
//  PersonBuilderTests.swift
//  issue_counterTests
//
//  Created by aitovd.r@gmail.com on 16/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import XCTest
@testable import issue_counter

class PersonBuilderTests: XCTestCase {
    
    func testBuildPerson() {
        let person = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
        ])
        
        XCTAssertNotNil(person, "Person should create normaly")
        XCTAssertEqual(person!.name, "Bob")
        XCTAssertEqual(person!.surname, "Dillan")
        XCTAssertEqual(person!.numberOfIssues, 9)
        XCTAssertEqual(person!.dateOfBirth, Date(timeIntervalSince1970: 252547200))
    }
    
    func testBuildPersonDictionaryWithAdditionalFields() {
        let person = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00",
            "dateOfDeathKey" : "1978-01-02T00:00:00"
            ])
        
        XCTAssertNotNil(person, "Person should create normaly")
        XCTAssertEqual(person!.name, "Bob")
        XCTAssertEqual(person!.surname, "Dillan")
        XCTAssertEqual(person!.numberOfIssues, 9)
        XCTAssertEqual(person!.dateOfBirth, Date(timeIntervalSince1970: 252547200))
    }
    
    func testBuildPersonIncompleteDictionary() {
        let person1 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ])
        
        XCTAssertNil(person1, "Person should be nil since name field is missing")
        
        let person2 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ])
        
        XCTAssertNil(person2, "Person should be nil since surname field is missing")
        
        let person3 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ])
        
        XCTAssertNil(person3, "Person should be nil since issue count field is missing")
        
        let person4 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            ])
        
        XCTAssertNil(person4, "Person should be nil since date of birth field is missing")
    }
    
    func testWrongTypeInIssueCountAndDateOfBirthFields() {
        let person1 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "abc",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ])
        
        XCTAssertNil(person1, "Person should be nil since issue count field has wrong type")
        
        let person2 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "abc9",
            PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ])
        
        XCTAssertNil(person2, "Person should be nil since issue count field has wrong type")
        
        let person3 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "abc"
            ])
        
        XCTAssertNil(person3, "Person should be nil since issue count field has wrong type")
        
        let person4 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "abc1978-01-02T00:00:00"
            ])
        
        XCTAssertNil(person4, "Person should be nil since issue count field has wrong type")
        
        let person5 = PersonBuilder.buildPerson(dictionary: [
            PersonBuilder.nameKey : "Bob",
            PersonBuilder.surnameKey : "Dillan",
            PersonBuilder.issueCountKey : "9",
            PersonBuilder.dateOfBirthKey : "1978"
            ])
        
        XCTAssertNil(person5, "Person should be nil since issue count field has wrong type")
    }
    
    func testBuildPersons() {
        let persons = PersonBuilder.buildPersons(dictionaries: [
            [
                PersonBuilder.nameKey : "Bob",
                PersonBuilder.surnameKey : "Dillan",
                PersonBuilder.issueCountKey : "9",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ],
            [
                PersonBuilder.nameKey : "Kenny",
                PersonBuilder.surnameKey : "West",
                PersonBuilder.issueCountKey : "5",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ],
            [
                PersonBuilder.nameKey : "Bob",
                PersonBuilder.surnameKey : "Marley",
                PersonBuilder.issueCountKey : "3",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ]
        ])
        
        XCTAssertEqual(persons.count, 3)
        
        XCTAssertEqual(persons[0].name, "Bob")
        XCTAssertEqual(persons[0].surname, "Dillan")
        XCTAssertEqual(persons[0].numberOfIssues, 9)
        XCTAssertEqual(persons[0].dateOfBirth, Date(timeIntervalSince1970: 252547200))
        
        XCTAssertEqual(persons[1].name, "Kenny")
        XCTAssertEqual(persons[1].surname, "West")
        XCTAssertEqual(persons[1].numberOfIssues, 5)
        XCTAssertEqual(persons[1].dateOfBirth, Date(timeIntervalSince1970: 252547200))
        
        XCTAssertEqual(persons[2].name, "Bob")
        XCTAssertEqual(persons[2].surname, "Marley")
        XCTAssertEqual(persons[2].numberOfIssues, 3)
        XCTAssertEqual(persons[2].dateOfBirth, Date(timeIntervalSince1970: 252547200))
    }
    
    func testBuildPersonsPartialFaulty() {
        let persons = PersonBuilder.buildPersons(dictionaries: [
            [
                PersonBuilder.nameKey : "Bob",
                PersonBuilder.surnameKey : "Dillan",
                PersonBuilder.issueCountKey : "9",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ],
            [
                PersonBuilder.nameKey : "Kenny",
                PersonBuilder.surnameKey : "West",
                PersonBuilder.issueCountKey : "9",
                PersonBuilder.dateOfBirthKey : ""
            ],
            [
                PersonBuilder.nameKey : "Kenny",
                PersonBuilder.surnameKey : "West",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ],
            [
                PersonBuilder.nameKey : "Mickey",
                PersonBuilder.surnameKey : "Mouse",
                PersonBuilder.issueCountKey : "3",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00",
                "dateOfDeathKey" : "1978-01-02T00:00:00"
            ],
            [
                PersonBuilder.nameKey : "Bob",
                PersonBuilder.surnameKey : "Marley",
                PersonBuilder.issueCountKey : "3",
                PersonBuilder.dateOfBirthKey : "1978-01-02T00:00:00"
            ]
            ])
        
        XCTAssertEqual(persons.count, 3)
        
        XCTAssertEqual(persons[0].name, "Bob")
        XCTAssertEqual(persons[0].surname, "Dillan")
        XCTAssertEqual(persons[0].numberOfIssues, 9)
        XCTAssertEqual(persons[0].dateOfBirth, Date(timeIntervalSince1970: 252547200))
        
        XCTAssertEqual(persons[1].name, "Mickey")
        XCTAssertEqual(persons[1].surname, "Mouse")
        XCTAssertEqual(persons[1].numberOfIssues, 3)
        XCTAssertEqual(persons[1].dateOfBirth, Date(timeIntervalSince1970: 252547200))
        
        XCTAssertEqual(persons[2].name, "Bob")
        XCTAssertEqual(persons[2].surname, "Marley")
        XCTAssertEqual(persons[2].numberOfIssues, 3)
        XCTAssertEqual(persons[2].dateOfBirth, Date(timeIntervalSince1970: 252547200))
    }
}
