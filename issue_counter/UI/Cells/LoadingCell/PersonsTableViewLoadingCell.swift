//
// Created by aitovd.r@gmail.com on 19/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

class PersonsTableViewLoadingCell : UITableViewCell {
    var viewModel : PersonsTableViewLoadingCellViewModelProtocol? {
        didSet {
            var oldViewModel = oldValue
            oldViewModel?.listener = nil
            viewModel?.listener = self
            updateSubviews()
        }
    }

    private var finishLoadingLabel : UILabel!
    private var loadingLabel : UILabel!
    private var loadingIndicator : UIActivityIndicatorView!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.setupSubviews()
        self.setupConstraints()
        self.setupAccessibilityIdentifiers()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setupSubviews() {
        selectionStyle = .none
        preservesSuperviewLayoutMargins = false
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
        
        finishLoadingLabel = UILabel()
        finishLoadingLabel.translatesAutoresizingMaskIntoConstraints = false;
        finishLoadingLabel.textAlignment = .center;
        addSubview(finishLoadingLabel)

        loadingLabel = UILabel()
        loadingLabel.translatesAutoresizingMaskIntoConstraints = false;
        loadingLabel.textAlignment = .center;
        addSubview(loadingLabel)
        
        loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false;
        addSubview(loadingIndicator)
    }

    func setupConstraints() {
        heightAnchor.constraint(equalToConstant: 60).isActive = true

        finishLoadingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        finishLoadingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        finishLoadingLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        loadingLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loadingLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        loadingIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loadingIndicator.trailingAnchor.constraint(equalTo: loadingLabel.leadingAnchor, constant: -8).isActive = true
    }

    func setupAccessibilityIdentifiers() {

    }

    func updateSubviews() {
        guard let viewModel = viewModel else {
            return
        }

        finishLoadingLabel.attributedText = viewModel.loadingFinishString
        loadingLabel.attributedText = viewModel.loadingString

        if viewModel.isLoading == true {
            finishLoadingLabel.isHidden = true
            loadingLabel.isHidden = false
            loadingIndicator.isHidden = false
            loadingIndicator.startAnimating()
        } else {
            finishLoadingLabel.isHidden = false
            loadingLabel.isHidden = true
            loadingIndicator.isHidden = true
            loadingIndicator.stopAnimating()
        }
    }
}

extension PersonsTableViewLoadingCell : ViewModelListener {
    func didUpdateViewModel() {
        DispatchQueue.main.async {
            self.updateSubviews()
        }
    }
}
