//
// Created by aitovd.r@gmail.com on 17/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

class PersonsTableViewCell : UITableViewCell {

    var viewModel : PersonsTableViewCellViewModelProtocol? {
        didSet {
            var oldViewModel = oldValue
            oldViewModel?.listener = nil
            viewModel?.listener = self
            updateSubviews()
        }
    }

    private var fullNameLabel : UILabel!
    private var dateOfBirthLabel : UILabel!
    private var issueCountLabel : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupSubviews()
        self.setupConstraints()
        self.setupAccessibilityIdentifiers()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setupSubviews() {
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        preservesSuperviewLayoutMargins = false
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
        
        fullNameLabel = UILabel()
        fullNameLabel.translatesAutoresizingMaskIntoConstraints = false;
        fullNameLabel.textAlignment = .left;
        addSubview(fullNameLabel)

        dateOfBirthLabel = UILabel()
        dateOfBirthLabel.translatesAutoresizingMaskIntoConstraints = false;
        dateOfBirthLabel.textAlignment = .left;
        addSubview(dateOfBirthLabel)

        issueCountLabel = UILabel()
        issueCountLabel.textColor = UIColor.white;
        issueCountLabel.translatesAutoresizingMaskIntoConstraints = false;
        issueCountLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        issueCountLabel.textAlignment = .center;
        issueCountLabel.layoutMargins = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)

        issueCountLabel.backgroundColor = UIColor.red
        issueCountLabel.layer.cornerRadius = 15.0;
        issueCountLabel.layer.masksToBounds = true;

        addSubview(issueCountLabel)
    }

    func setupConstraints() {
        heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        fullNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        fullNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true

        dateOfBirthLabel.leadingAnchor.constraint(equalTo: fullNameLabel.leadingAnchor).isActive = true
        dateOfBirthLabel.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: 4).isActive = true
        dateOfBirthLabel.trailingAnchor.constraint(equalTo: fullNameLabel.trailingAnchor).isActive = true

        issueCountLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true
        issueCountLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        issueCountLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        issueCountLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40).isActive = true
        issueCountLabel.leadingAnchor.constraint(equalTo: fullNameLabel.trailingAnchor, constant: 16).isActive = true
        issueCountLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
    }

    func setupAccessibilityIdentifiers() {

    }

    func updateSubviews() {
        guard let viewModel = viewModel else {
            return
        }
        
        fullNameLabel.attributedText = viewModel.fullName
        dateOfBirthLabel.attributedText = viewModel.dateOfBirth
        issueCountLabel.attributedText = viewModel.numberOfIssues
        issueCountLabel.backgroundColor = viewModel.colorOfIssues
    }
}

extension PersonsTableViewCell : ViewModelListener {
    func didUpdateViewModel() {
        DispatchQueue.main.async {
            self.updateSubviews()
        }
    }
}
