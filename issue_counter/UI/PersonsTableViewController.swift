//
//  PersonsTableViewController.swift
//  issue_counter
//
//  Created by aitovd.r@gmail.com on 16/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

class PersonsTableViewController: UITableViewController {

    private static let personCellIdentifier = "PersonsTableViewCell"
    private static let loadingCellIdentifier = "PersonsTableViewLoadingCell"
    private static let minimumSearchFilterLength = 2

    private var viewModel : PersonsTableViewControllerViewModelProtocol
    private let searchController = UISearchController(searchResultsController: nil)


    init(viewModel: PersonsTableViewControllerViewModelProtocol) {
        self.viewModel = viewModel

        super.init(style: .plain)

        self.viewModel.listener = self
        self.tableView.register(PersonsTableViewCell.classForCoder(), forCellReuseIdentifier: PersonsTableViewController.personCellIdentifier)
        self.tableView.register(PersonsTableViewLoadingCell.classForCoder(), forCellReuseIdentifier: PersonsTableViewController.loadingCellIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupSubviews()
        self.setupConstraints()
        self.setupAccessibilityIdentifiers()
    }

    func setupSubviews() {
        navigationItem.title = viewModel.title.string
        tableView.tableFooterView = UIView()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Persons"
        searchController.searchBar.delegate = self
        searchController.searchBar.searchBarStyle = UISearchBarStyle.minimal
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    func setupConstraints() {

    }

    func setupAccessibilityIdentifiers() {

    }
}

extension PersonsTableViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            if  searchText.count > PersonsTableViewController.minimumSearchFilterLength {
                viewModel.filterPersonsForSearchString(searchController.searchBar.text)
            }
        }
    }
}

extension PersonsTableViewController : UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.filterPersonsForSearchString(nil)
    }
}

extension PersonsTableViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= viewModel.numberOfPersons - 1 {
            viewModel.getNextPersons()
        }
    }
}

extension PersonsTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfPersons + 1
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == viewModel.numberOfPersons {
            let cellModel = viewModel.modelForLoadingCell()
            let cell = tableView.dequeueReusableCell(withIdentifier: PersonsTableViewController.loadingCellIdentifier, for: indexPath) as! PersonsTableViewLoadingCell

            cell.viewModel = cellModel
            return cell
        } else {
            var cellModel = viewModel.modelForCell(at: indexPath.row)
            cellModel.filter = viewModel.filter
            let cell = tableView.dequeueReusableCell(withIdentifier: PersonsTableViewController.personCellIdentifier, for: indexPath) as! PersonsTableViewCell

            cell.viewModel = cellModel
            return cell
        }
    }
}

extension PersonsTableViewController : ViewModelListener {
    func didUpdateViewModel() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
