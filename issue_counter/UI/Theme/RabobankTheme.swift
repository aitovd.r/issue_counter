//
//  RabobankTheme.swift
//  issue_counter
//
//  Created by aitovd.r@gmail.com on 20/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

class RabobankTheme {
    static let color1 = UIColor(red: 250.0/255.0, green: 100.0/255.0, blue: 33.0/255.0, alpha: 1.0) //orange
    static let color2 = UIColor(red: 3.0/255.0, green: 16.0/255.0, blue: 150.0/255.0, alpha: 1.0) //blue
    static let color3 = UIColor(red: 129.0/255.0, green: 184.0/255.0, blue: 53.0/255.0, alpha: 1.0) //green
    static let color4 = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0) //gray
    static let color5 = UIColor.white
    
    static let normalTextSize : CGFloat = 18.0
    static let smallTextSize : CGFloat = 14.0
    
    static func setupAppearance() {
        UIApplication.shared.statusBarStyle = .lightContent
        
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: color5]
        UINavigationBar.appearance().barTintColor = color1
        UINavigationBar.appearance().tintColor = color5
        
        UISearchBar.appearance().barTintColor = color5
        UISearchBar.appearance().tintColor = color5
    }
}
