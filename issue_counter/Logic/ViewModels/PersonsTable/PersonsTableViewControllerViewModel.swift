//
// Created by aitovd.r@gmail.com on 17/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import Foundation

class PersonsTableViewControllerViewModel : PersonsTableViewControllerViewModelProtocol {
    var listener: ViewModelListener?

    var title: NSAttributedString {
        return NSAttributedString(string: "Persons")
    }

    var numberOfPersons: Int {
        return filter != nil ? filteredPersons.count : persons.count
    }

    private var persons = [Person]()
    private var filteredPersons = [Person]()
    private(set) var filter : String?
    private let parser : CSVParser
    private var isFileFullyParsed = false
    private var isLoading = false
    private let loadingCellModel = PersonsTableViewLoadingCellViewModel(loadingString: "Loading", loadingFinishString: "No more results")
    private let searchingCellModel = PersonsTableViewLoadingCellViewModel(loadingString: "Searching", loadingFinishString: "No more search results")

    required init(file: URL) {
        self.parser = CSVParser(file: file, listener: nil)
        do {
            self.parser.listener = self
            try self.parser.parse()
        } catch {

        }
    }

    func filterPersonsForSearchString(_ filter: String?) {
        let newFilter = self.filter != filter
        let countOfPersonsBefore = filteredPersons.count
        
        self.filter = filter
        
        if let filter = self.filter {
            filteredPersons = persons.filter{
                $0.name.lowercased().contains(filter.lowercased()) || $0.surname.lowercased().contains(filter.lowercased())
            }
        } else {
            filteredPersons = []
        }
        
        if (newFilter || filteredPersons.count != countOfPersonsBefore)
        {
            listener?.didUpdateViewModel()
        } else {
            getNextPersons()
        }
    }

    func modelForCell(at index: Int) -> PersonsTableViewCellViewModelProtocol {
        return PersonsTableViewCellViewModel(person: filter != nil ? filteredPersons[index] : persons[index])
    }

    func modelForLoadingCell() -> PersonsTableViewLoadingCellViewModelProtocol {
        return filter != nil ? searchingCellModel : loadingCellModel
    }

    func getNextPersons() {
        if isFileFullyParsed == false && isLoading == false {
            isLoading = true
            self.parser.next()
        }
    }
}

extension PersonsTableViewControllerViewModel : CSVParserListener {
    func parser(_ parser: CSVParser, didParseHeaders headers: [String]) {

    }

    func parser(_ parser: CSVParser, didParseRows rows: [[String : String]]) {
        isLoading = false
        let countOfPersonsBefore = persons.count
        
        autoreleasepool {
            persons.append(contentsOf: PersonBuilder.buildPersons(dictionaries: rows))
        }
        
        if filter != nil {
            filterPersonsForSearchString(filter)
        } else if self.persons.count != countOfPersonsBefore {
            listener?.didUpdateViewModel()
        }
    }

    func parser(_ parser: CSVParser, didFinishParsingFile file: URL) {
        isFileFullyParsed = true
        searchingCellModel.isLoading = false
        loadingCellModel.isLoading = false
        listener?.didUpdateViewModel()
    }

    func parser(_ parser: CSVParser, didStartParsingFile file: URL) {

    }
}
