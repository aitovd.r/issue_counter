//
// Created by aitovd.r@gmail.com on 17/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import Foundation

protocol PersonsTableViewControllerViewModelProtocol: ViewModelProtocol, CSVParserListener {
    var title : NSAttributedString {get}
    var numberOfPersons : Int {get}
    var filter : String? {get}

    init(file: URL)

    func filterPersonsForSearchString(_ search: String?)

    func modelForCell(at: Int) -> PersonsTableViewCellViewModelProtocol
    func modelForLoadingCell() -> PersonsTableViewLoadingCellViewModelProtocol

    func getNextPersons()
}
