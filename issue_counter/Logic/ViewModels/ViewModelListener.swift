//
// Created by aitovd.r@gmail.com on 17/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import Foundation

protocol ViewModelListener {
    func didUpdateViewModel()
}
