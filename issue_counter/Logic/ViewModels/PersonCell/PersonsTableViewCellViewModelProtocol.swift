//
// Created by aitovd.r@gmail.com on 17/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

protocol PersonsTableViewCellViewModelProtocol : ViewModelProtocol {
    var fullName : NSAttributedString {get}
    var dateOfBirth : NSAttributedString {get}
    var numberOfIssues : NSAttributedString {get}
    var colorOfIssues : UIColor {get}

    var filter : String? {get set}
    
    init(person : Person)
}
