//
// Created by aitovd.r@gmail.com on 17/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

//TODO Move constants and colors to theme class
class PersonsTableViewCellViewModel : PersonsTableViewCellViewModelProtocol {
    var fullName: NSAttributedString {
        let fullName = person.name + " " + person.surname
        let result = NSMutableAttributedString(string: fullName, attributes: [
            NSAttributedStringKey.foregroundColor: RabobankTheme.color2,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: RabobankTheme.normalTextSize)
        ])
        
        if let filter = filter {
            result.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: RabobankTheme.normalTextSize), range: (fullName as NSString).range(of: filter))
        }
        
        return result
    }

    var dateOfBirth: NSAttributedString {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"

        return NSAttributedString(string: dateFormatter.string(from: person.dateOfBirth), attributes: [
            NSAttributedStringKey.foregroundColor: RabobankTheme.color4,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: RabobankTheme.smallTextSize)
        ])
    }

    var numberOfIssues: NSAttributedString {
        return NSAttributedString(string: "\(person.numberOfIssues)")
    }

    var colorOfIssues: UIColor {
        switch person.numberOfIssues {
        case 0..<3:
            return RabobankTheme.color3
        case 3...5:
            return RabobankTheme.color2
        default:
            return RabobankTheme.color1
        }
    }
    
    var listener: ViewModelListener?
    var filter: String?
    
    private let person: Person

    required init(person: Person) {
        self.person = person
    }
}
