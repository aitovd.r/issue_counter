//
// Created by aitovd.r@gmail.com on 19/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

class PersonsTableViewLoadingCellViewModel : PersonsTableViewLoadingCellViewModelProtocol {
    var listener: ViewModelListener?

    var isLoading = true {
        didSet {
            listener?.didUpdateViewModel()
        }
    }
    var loadingString: NSAttributedString
    var loadingFinishString: NSAttributedString

    init(loadingString: String, loadingFinishString: String) {
        self.loadingString = NSAttributedString(string: loadingString, attributes: [
            NSAttributedStringKey.foregroundColor: RabobankTheme.color1,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: RabobankTheme.normalTextSize)
        ])

        self.loadingFinishString = NSAttributedString(string: loadingFinishString, attributes: [
            NSAttributedStringKey.foregroundColor: RabobankTheme.color1,
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: RabobankTheme.normalTextSize)
        ])
    }
}
