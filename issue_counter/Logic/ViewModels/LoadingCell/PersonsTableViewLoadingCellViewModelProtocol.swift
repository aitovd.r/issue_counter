//
// Created by aitovd.r@gmail.com on 19/11/2017.
// Copyright (c) 2017 Dmitrii Aitov. All rights reserved.
//

import Foundation

protocol PersonsTableViewLoadingCellViewModelProtocol : ViewModelProtocol {
    var isLoading : Bool {get}
    var loadingString : NSAttributedString {get}
    var loadingFinishString: NSAttributedString {get}
}
