//
//  CSVParser.swift
//  issue_counter
//
//  Created by aitovd.r@gmail.com on 16/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit

protocol CSVParserListener {
    
    func parser(_ parser: CSVParser, didParseHeaders headers: [String])
    
    func parser(_ parser: CSVParser, didParseRows rows: [[String:String]])
    
    func parser(_ parser: CSVParser, didFinishParsingFile file: URL)
    
    func parser(_ parser: CSVParser, didStartParsingFile file: URL)
}

enum CSVParserError : Error {
    case notFileURL
    case cantReadFile
}

class CSVParser {
    let file : URL
    var listener : CSVParserListener?
    let chunkSize : Int
    var data : Data!
    var readPointer = 0
    
    private var headers = [String]()

    init(file : URL, listener : CSVParserListener?, chunkSize : Int = 16000) {
        self.file = file
        self.listener = listener
        self.chunkSize = chunkSize
    }
    
    func parse() throws {
        guard file.isFileURL else {
            throw CSVParserError.notFileURL
        }

        headers = []
        readPointer = 0
        do {
            data = try Data(contentsOf: self.file, options: .dataReadingMapped)
        } catch {
            throw CSVParserError.cantReadFile
        }

        self.listener?.parser(self, didStartParsingFile: self.file)
        self.next()
    }

    func next() {
        DispatchQueue.global(qos: .userInitiated).async {
            let stopReadPointer = (self.readPointer + self.chunkSize < self.data.count) ? self.readPointer + self.chunkSize : self.data.count
            let subdata = self.data.subdata(in: self.readPointer..<stopReadPointer)
            var chunk = String(data: subdata, encoding: .utf8)
            
            if (stopReadPointer < self.data.count) {
                let rangeOfLastNewLine = chunk?.rangeOfCharacter(from: CharacterSet.newlines, options: .backwards, range: nil)
                chunk = chunk?.substring(to: (rangeOfLastNewLine?.lowerBound)!)
            }
            
            if let chunk = chunk {
                self.readPointer = self.readPointer + chunk.lengthOfBytes(using: .utf8);
                self.parse(chunk: chunk)
                
                if self.readPointer == self.data.count {
                    self.listener?.parser(self, didFinishParsingFile: self.file)
                }
            }
        }
    }

    private func parse(chunk : String) {
        var result = [[String:String]]()
        
        let rows = rowsFrom(string: chunk)

        for row in rows {
            let columns = columnsFrom(string: row)
            if headers.count == 0 {
                headers = columns
                listener?.parser(self, didParseHeaders: headers)
            } else if validate(columns: columns) {
                var rowDictionary = [String:String]()
                for index in 0..<headers.count {
                    autoreleasepool {
                        rowDictionary[headers[index]] = columns[index]
                    }
                }
                result.append(rowDictionary)
            }
        }
        
        if result.count > 0 {
            self.listener?.parser(self, didParseRows: result)
        }
    }
    
    private func rowsFrom(string : String) -> [String] {
        var rows = [String]()
        autoreleasepool {
            let delimiter = "\n"
            var cleanString = string
            cleanString = cleanString.replacingOccurrences(of: "\r", with: delimiter)
            cleanString = cleanString.replacingOccurrences(of: "\n\n", with: delimiter)
            rows = cleanString.components(separatedBy: delimiter)
        }
        
        return rows
    }
    
    private func columnsFrom(string : String) -> [String] {
        var columns = [String]()
        autoreleasepool {
            let delimiter = "\t"
            var cleanString = string
            cleanString = cleanString.replacingOccurrences(of: "\",\"", with: delimiter)
            cleanString = cleanString.replacingOccurrences(of: ",\"", with: delimiter)
            cleanString = cleanString.replacingOccurrences(of: "\",", with: delimiter)
            cleanString = cleanString.replacingOccurrences(of: "\"", with: "")
            columns = cleanString.components(separatedBy: delimiter)
        }
        
        return columns
    }
    
    private func validate(columns : [String]) -> Bool {
        return columns.count == headers.count
    }
}
