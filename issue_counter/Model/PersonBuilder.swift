//
//  PersonBuilder.swift
//  issue_counter
//
//  Created by aitovd.r@gmail.com on 16/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import Foundation

class PersonBuilder {
    static let nameKey = "First name"
    static let surnameKey = "Sur name"
    static let issueCountKey = "Issue count"
    static let dateOfBirthKey = "Date of birth"
    
    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "GMT") as TimeZone!
        return formatter
    }()
    
    static func buildPerson(dictionary : [String:String]) -> Person? {
        guard dictionary.contains(where: { $0.key == nameKey })
            && dictionary.contains(where: { $0.key == surnameKey })
            && dictionary.contains(where: { $0.key == issueCountKey })
            && dictionary.contains(where: { $0.key == dateOfBirthKey }) else {
            return nil
        }
        
        let name = dictionary[nameKey]!
        let surname = dictionary[surnameKey]!
        
        let issueCount = UInt(dictionary[issueCountKey]!)
        let dateOfBirth = dateFormatter.date(from: dictionary[dateOfBirthKey]!)
        
        if let issueCountR = issueCount, let dateOfBirthR = dateOfBirth {
            return Person(name: name, surname: surname, dateOfBirth: dateOfBirthR, numberOfIssues: issueCountR)
        } else {
            return nil
        }
    }
    
    static func buildPersons(dictionaries :[[String:String]]) -> [Person] {
        
        var result = [Person]()
        
        for personDictionary in dictionaries {
            if let person = buildPerson(dictionary: personDictionary) {
                result.append(person)
            }
        }
        
        return result
    }
}
