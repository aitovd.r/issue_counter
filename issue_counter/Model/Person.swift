//
//  Person.swift
//  issue_counter
//
//  Created by aitovd.r@gmail.com on 16/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import Foundation

struct Person {
    let name : String
    let surname : String
    let dateOfBirth : Date
    var numberOfIssues : UInt
    
    func log() {
        print(name + " " + surname)
    }
}
