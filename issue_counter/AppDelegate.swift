//
//  AppDelegate.swift
//  issue_counter
//
//  Created by aitovd.r@gmail.com on 16/11/2017.
//  Copyright © 2017 Dmitrii Aitov. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.

        self.window = UIWindow(frame: UIScreen.main.bounds)

        RabobankTheme.setupAppearance()
        
        if let issuesFileUrl = Bundle.main.url(forResource: "issues-big", withExtension: "csv") {
            let personsViewModel = PersonsTableViewControllerViewModel(file: issuesFileUrl)
            let personsController = PersonsTableViewController(viewModel: personsViewModel)
            let navigationController = UINavigationController(rootViewController: personsController)

            self.window?.rootViewController = navigationController
        }

        self.window?.makeKeyAndVisible()

        return true
    }
}

